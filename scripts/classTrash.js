/* Importation des paramètres nécessaires à l'exécution de classTrash.js */
import { canvas, ctx } from "./canvas.js";
import { addPoint, hitSound, overSound, healthSound } from "./sounds.js";
import { addPlayer, playerName } from "./localStorage.js";
import { globalSettings, size, cTenth } from "./setting.js";
import { setup } from "./setup.js";
import { playerImage } from "./images.js";

/* Déclaration et exportation des variables de sélectionner des éléments du DOM */
export let selectBonus = document.querySelector(".bonus");
export let counterBonus = document.querySelector(".full-bag");
export let sacRemplissage = document.querySelector(".remplissage");

/* Création et exportation de la classe Trash */
export class Trash {
  constructor(
    name, // Nom du trash
    gap, // Distance entre deux mêmes trash
    height, // Hauteur de l'image du trash
    width, // Largeur de l'image du trash
    imgsrc, // Source de l'image du trash
    globalSettings, // Création de l'array vide du trash qui sera rempli lors du mapping
    arrayLenght, // Longueur du tableau pour le mapping
    damage // Nombre de dégats que fait le trash
  ) {
    (this.name = name),
      (this.gap = gap),
      (this.height = height),
      (this.width = width),
      (this.imgsrc = imgsrc),
      (this.globalSettings = globalSettings),
      (this.arrayLenght = arrayLenght),
      (this.damage = damage);
  }

  /* Méthode pour la création d'un trash avec une méthode de mapping du tableau */
  createTrash() {
    this.globalSettings = Array(this.arrayLenght) // On crée un array d'un nombre voulu de trash
      .fill()
      .map((a, i) => [
        canvas.width + this.gap * this.width + i * this.gap * this.width, // distance en x entre 2 déchets consécutifs qui seront map dans l'écran de jeu
        Math.random() * (canvas.height - this.height), // position en y d'un déchet déterminée de manière aléatoire dans l'écran de jeu
      ]);
    return this.globalSettings; // récupération du tableau
  }

  /* Méthode utilisée pour recrée des trash dans leur array respectif lorsque ces derniers sortent de l'écran */
  takeTrash() {
    this.globalSettings = [
      ...this.globalSettings.slice(1), // suppression du premier élément de l'array => suppression de l'écran
      [
        this.globalSettings[this.globalSettings.length - 1][0] +
          this.gap * this.width, // on crée un nouveau objet dans le tableau en x
        Math.random() * (canvas.height - this.height), // on crée un nouveau objet dans le tableau en y
      ],
    ];
  }

  /* Méthode utilisée lorsqu'on a "pris" le sac et quand on touche des trash (mode bonus) */
  takeTrashBonus() {
    globalSettings.countItems++; // On ajoute 1 pour chaque objet ramassé dans la variable countItems
    // Effets de style pour remplir le sac (1 objet = 20% de remplissage)
    if (globalSettings.countItems == 1) {
      sacRemplissage.style.height = "80%";
    } else if (globalSettings.countItems == 2) {
      sacRemplissage.style.height = "60%";
    } else if (globalSettings.countItems == 3) {
      sacRemplissage.style.height = "40%";
    } else if (globalSettings.countItems == 4) {
      sacRemplissage.style.height = "20%";
    } else if (globalSettings.countItems == 5) {
      sacRemplissage.style.height = "0%";
    }

    this.globalSettings = [
      ...this.globalSettings.slice(1), // suppression du premier élément de l'array => suppression de l'écran
      [
        this.globalSettings[this.globalSettings.length - 1][0] +
          this.gap * this.width, // on crée un nouveau objet dans le tableau en x
        Math.random() * (canvas.height - this.height), // on crée un nouveau objet dans le tableau en y
      ],
    ];

    if (globalSettings.countItems === 5) {
      // sac rempli à 100% = 5 items récupérés
      counterBonus.innerHTML = "Sac plein ! <br/> + 50"; // on affiche du texte à l'écran
      setTimeout(function () {
        counterBonus.innerHTML = "";
      }, 3000); // on vide le texte au bout de 3 secondes
      globalSettings.currentScore = globalSettings.currentScore + 50; // on ajoute 50 points au joueur
      globalSettings.countItems = 0; // on réinitialise le compteur d'items et sa jauge
      sacRemplissage.style.height = "100%";
    }
  }

  // Méthode pour l'affichage d'un trash après réalisation du mapping
  drawTrash() {
    this.globalSettings.map((element) => {
      // on map chaque élement du tableau à l'écran
      element[0] -= globalSettings.speed; // Position en x des dechet vaut -speed
      ctx.drawImage(
        this.imgsrc, // Source de l'image
        0, // Hauteur de l'image
        0, // Largeur du l'image
        this.width, // Largeur de l'image
        this.height, // Hauteur de l'image
        element[0], // Position X du trash
        element[1], // Position Y du trash
        this.width, // Position du canvas
        this.height // Position du canvas
      );

      // Si la position en x du trash sort de l'écran à gauche
      if (element[0] <= -this.width) {
        globalSettings.currentScore++; // On rajoute +1 au score à objet qui sort de l'écran
        addPoint.play(); // On joue un son lorsqu'on gagne 1 point
        globalSettings.bestScore = Math.max(
          globalSettings.bestScore,
          globalSettings.currentScore
        ); // On compare le best score et le current score pour garder seulement le meilleur des deux à chaque frame
        this.takeTrash(); // A chaque trash qui sort de l'écran, on applique la méthode correspondante
      }
      // Si le joueur touche un trash => Il perd de la vie
      if (
        element[0] <= cTenth + size && // 1ère condition en x
        element[0] + this.width >= cTenth && // 2ème condition en x
        element[1] <= globalSettings.flyHeight + size / 2 && // 1ère condition en y
        element[1] + this.height >= globalSettings.flyHeight // 2ème condition en y
      ) {
        if (this.name === "Leaf") {
          // Si l'objet est une feuille on ajoute des points de vie
          if (globalSettings.currentLife < 100) {
            globalSettings.currentLife += this.damage;
            healthSound.play(); // On joue le son d'ajout de point de vie
            if (globalSettings.currentLife > 100) {
              // si la vie est > 100 après avoir pris une feuille, on reaffecte la valeur de 100 à la vie
              globalSettings.currentLife = 100;
            }
          }
        } else if (!globalSettings.isCarryingBag) {
          // Si le joueur n'a pas en sa possession un sac
          globalSettings.currentLife -= this.damage; // On retire les points de vie avec la valeur de dégats
          hitSound.play(); // On joue le son quand on touche un objet
        } else if (globalSettings.isCarryingBag) {
          // Si le jour est en possession d'un sac
          this.takeTrashBonus(); // On applique la méthode correspondante
        }

        if (this.name == "Bag") {
          // Si le joueur touche un sac
          this.takeTrash(); // On applique la méthode correspondante
          globalSettings.isCarryingBag = true;
          playerImage.src = "./assets/img/mermaidcontaminate.png"; // Changement de la source de l'image
          selectBonus.innerHTML = "Recyclez !"; // Apparition d'un message à l'écran
          setTimeout(function () {
            // Au bout de 5 secondes, on rétablit les paramètres initiaux au personnage et on vide le message
            globalSettings.isCarryingBag = false;
            playerImage.src = "./assets/img/mermaid.png";
            selectBonus.innerHTML = "";
          }, 5000);
        }

        if (globalSettings.currentLife <= 0) {
          // Si la vie du personnage est à 0
          overSound.play(); // Son de game over
          selectBonus.innerHTML = ""; // On vide le message de reyclage
          addPlayer(playerName, globalSettings.bestScore); // On ajoute au local storage le player avec son score
          globalSettings.gamePlaying = false; // Fin de la partie
          setup(); // On lance le setup pour recharger le menu principal
        }
      }
    });
  }
}

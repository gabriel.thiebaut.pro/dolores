/* Importation des paramètres nécessaires à l'exécution de render.js */
import { globalSettings, bottle, beer, pizza, bag, leaf } from "./setting.js";
import { updateScore } from "./score.js";
import { backgroundMusic } from "./sounds.js";
import { drawBackground } from "./drawBackground.js";
import { drawDoloresMenu, drawDoloresPlay } from "./drawDolores.js";
import { backgroundImage } from "./images.js";
import { levelsGame } from "./levels.js";
import { canvas, ctx } from "./canvas.js";

/* Déclaration d'une constante render au démarrage du jeu */
const render = () => {
  globalSettings.index++; // l'index prend une valeur de +1 à chaque boucle de chargement
  ctx.clearRect(0, 0, canvas.width, canvas.height); // A chaque frame, on nettoie le canvas pour éviter l'effet de duplication et de trainée des objets
  // Si on est en train de jouer
  if (globalSettings.gamePlaying) {
    drawBackground();
    backgroundMusic.play(); // Music du jeu
    drawDoloresPlay(); // Affichage du joueur à gauche de l'écran
    bag.drawTrash(); // Affichage et génération des sacs
    bottle.drawTrash(); // Affichage et génération des bouteilles en plastique
    beer.drawTrash(); // Affichage et génération des bouteilles de bière
    pizza.drawTrash(); // Affichage et génération des cartons de pizzas
    leaf.drawTrash(); // Affichage et génération des feuilles
    levelsGame(); // Lancement de la fonction de génération des différents niveaux de jeu

    // Si on est sur le menu (par défaut)
  } else {
    drawBackground();
    globalSettings.speed = 6; // On remet la vitesse par défaut
    globalSettings.backgroundSpeed = 3; // De même pour le fond
    backgroundImage.src = "./assets/img/background.png"; // On remet l'image par défaut du fond
    drawDoloresMenu(); // On affiche le joueur au centre de l'écran
  }

  // On actualise les scores */
  updateScore();

  //  On dit au navigateur de réaliser les animations en relançant le render à chaque frame */
  window.requestAnimationFrame(render);
};

/* On exporte le render */
export { render };

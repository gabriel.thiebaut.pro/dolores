/* Importation des paramètres nécessaires à l'exécution de launch.js */
import { globalSettings, jump } from "./setting.js"; 
import { setup } from "./setup.js"; 
import { render } from "./render.js";
import { playerCreation } from "./localStorage.js";

/* Lancement du jeu */
function launchGame() {
  setup(); // On lance le setup
  window.onload = render(); // Au chargement de la fenêtre on lance le render
  playerCreation(); // On récupère la valeur de l'input name avant de lancer le jeu
  window.onclick = () => (globalSettings.flight = jump); // Au click on attribue la valeur du jump à flight
}

/* Exportation de la fonction launch */
export { launchGame };

/* Importation des paramètres nécessaires à l'exécution de setup.js */
import { globalSettings } from "./setting.js";
import { bottle, pizza, bag, beer, leaf } from "./setting.js";
import {
  form,
  recordButtonSelect,
  recordListSelect,
  gameLaunchSelector,
} from "./localStorage.js";

/* Déclaration d'une variable permettant de sélectionner la classe .remplissage (jauge du sac bonus) */
let sacRemplissage = document.querySelector(".remplissage");

/* Déclaration d'une constante setup lors du chargement du jeu */
const setup = () => {
  globalSettings.currentScore = 0; // Au chargement du jeu le score est de 0
  globalSettings.currentLife = 100; // Au chargement on remet la vie à 100
  globalSettings.gravity = 0.05; // Au chargement on remet la gravité à 0.05
  globalSettings.isCarryingBag = false;
  form.style.display = "block"; // Affichage des éléments cachés du menu
  recordButtonSelect.style.display = "block";
  recordListSelect.style.display = "flex";
  gameLaunchSelector.style.display = "block";
  globalSettings.countItems = 0;
  sacRemplissage.style.height = "100%";

  // Création d'un mapping pour chaque trash lors du setup comprenant 2 arguments => array
  bottle.createTrash();
  pizza.createTrash();
  bag.createTrash();
  beer.createTrash();
  leaf.createTrash();
};

/* Exportation du setup */
export { setup };

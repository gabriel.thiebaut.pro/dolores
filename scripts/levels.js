/* Importation des paramètres nécessaires à l'exécution de levels.js */
import { globalSettings } from "./setting.js";
import { backgroundImage } from "./images.js";
import { backgroundWidth } from "./setting.js";

const moduloMin = 5;

/* On exporte la fonction permettant de modifier la difficulté du jeu en fonction du score du joueur */
export function levelsGame () {
    switch (true) { // On vérifie le score du joueur
        case ((((globalSettings.index * globalSettings.backgroundSpeed) %
        backgroundWidth)) < moduloMin && globalSettings.currentScore >= 20 && globalSettings.currentScore <= 100) :
        globalSettings.speed = 7; // Modification de la vitesse des trash
        backgroundImage.src = "./assets/img/background2.png"; // Application d'un background différent
        break;
        case ((((globalSettings.index * globalSettings.backgroundSpeed) %
        backgroundWidth)) < moduloMin && globalSettings.currentScore >= 100 && globalSettings.currentScore <= 200) :
        globalSettings.speed = 8;
        globalSettings.backgroundSpeed = 4;
        backgroundImage.src = "./assets/img/background3.png";
        break;
        case ((((globalSettings.index * globalSettings.backgroundSpeed) %
        backgroundWidth)) < moduloMin && globalSettings.currentScore >= 200 && globalSettings.currentScore <= 300) :
        globalSettings.speed = 9;
        globalSettings.backgroundSpeed = 4.5;
        backgroundImage.src = "./assets/img/background4.png";
        break;
        case ((((globalSettings.index * globalSettings.backgroundSpeed) %
        backgroundWidth)) < moduloMin && globalSettings.currentScore >= 300 && globalSettings.currentScore <= 400) :
        globalSettings.speed = 10;
        globalSettings.backgroundSpeed = 5;
        backgroundImage.src = "./assets/img/background5.png";
        break;
        case ((((globalSettings.index * globalSettings.backgroundSpeed) %
        backgroundWidth)) < moduloMin && globalSettings.currentScore >= 400 && globalSettings.currentScore <= 500) :
        globalSettings.speed = 11;
        globalSettings.backgroundSpeed = 5.5;
        backgroundImage.src = "./assets/img/background6.png";
        break;
    }
} 

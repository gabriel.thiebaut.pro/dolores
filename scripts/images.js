/* Importation des images du projet avec l'objet Image JS */
const backgroundImage = new Image();
backgroundImage.src = "./assets/img/background.png";

const backgroundImage2 = new Image();
backgroundImage2.src = "./assets/img/background2.png";

const backgroundImage3 = new Image();
backgroundImage2.src = "./assets/img/background3.png";

const backgroundImage4 = new Image();
backgroundImage2.src = "./assets/img/background4.png";

const backgroundImage5 = new Image();
backgroundImage2.src = "./assets/img/background5.png";

const backgroundImage6 = new Image();
backgroundImage2.src = "./assets/img/background6.png";

const blackBagImage = new Image();
blackBagImage.src = "./assets/img/blackbag.png";

const playerImage = new Image();
playerImage.src = "./assets/img/mermaid.png";

const bottleImage = new Image();
bottleImage.src = "./assets/img/bottle.png";

const beerImage = new Image();
beerImage.src = "./assets/img/beer.png";

const pizzaImage = new Image();
pizzaImage.src = "./assets/img/pizza.png";

const leafImage = new Image();
leafImage.src = "./assets/img/leaf.png";

/* Exportation des images */
export {
  backgroundImage,
  backgroundImage2,
  backgroundImage3,
  backgroundImage4,
  backgroundImage5,
  backgroundImage6,
  blackBagImage,
  playerImage,
  bottleImage,
  beerImage,
  pizzaImage,
  leafImage,
};

/* Importation des paramètres nécessaires à l'exécution de localStorage.js */
import { globalSettings } from "./setting.js";

/* Déclaration des variables permettant de sélectionner des éléments du DOM */
let input = document.querySelector("#name");
let form = document.querySelector(".form");
let canvasSelect = document.querySelector("canvas");
let recordButtonSelect = document.querySelector(".record-button");
let recordListSelect = document.querySelector(".record-list");
let recordListNumberSelect = document.querySelector(".record-list-number");
let recordListNameSelect = document.querySelector(".record-list-name");
let recordListScoreSelect = document.querySelector(".record-list-score");
let gameLaunchSelector = document.querySelector(".game-launch");

/* Déclaration d'une variable playerName permettant de récupérer l'input "nom du joueur" */
let playerName;

/* Déclaration d'une variable permettant d'afficher les éléments du tableau du local storage ayant pour key le score (objets JSON) */
let scoreArray = JSON.parse(localStorage.getItem("Score")); // Besoin de parser les objects pour afficher du JSON

/* Si aucune valeur n'a été rentrée on crée un tableau vide */
if (scoreArray == null) {
  scoreArray = [];
}

/* On trie les scores par ordre décroissant */
scoreArray.sort(function (a, b) {
  return b.playerscore - a.playerscore;
});

/* On crée une fonction pour ajouter le nom et le score du joueur dans le tableau valeur */
function addPlayer(name, score) {
  scoreArray.push({
    playername: name,
    playerscore: score,
  });
  localStorage.setItem("Score", JSON.stringify(scoreArray)); // On effectue la modification du local storage en lui rajoutant un objet (JSON stringify)
}

/* On crée une fonction permettant de récupérer la valeur de l'input "nom du joueur" lors d'un click */
function playerCreation() {
  canvasSelect.addEventListener("click", (a) => {
    playerName = input.value;
    if (playerName === "") {
      // Si le champ est vide, on affiche un message d'erreur
      form.insertAdjacentHTML(
        "beforeend",
        '<div class="error">Veuillez entrer votre nom !</div>'
      );
    } else {
      let errorSelect = document.querySelectorAll(".error"); // Lors du lancement du jeu on supprime les erreurs
      errorSelect.forEach((error) => {
        error.remove();
      });
      // On fait disparaître les messages non désirés lorsqu'on joue
      form.style.display = "none";
      recordButtonSelect.style.display = "none";
      recordListSelect.style.display = "none";
      gameLaunchSelector.style.display = "none";
      globalSettings.gamePlaying = true;
    }
  });
}

/* Lorsqu'on click sur le bouton record, on affiche les score du local storage */
recordButtonSelect.addEventListener("click", function displayScore() {
  if (recordListSelect.classList.value === "record-list") {
    recordListSelect.classList.replace("record-list", "record-list-active"); // On modifie la classe pour afficher du style
    recordButtonSelect.style.color = "#4682B4";
    recordButtonSelect.style.borderBottom = "3px solid #4682B4";

    for (let i = 1; i <= 10; i++) {
      // Première boucle for pour afficher des nombres de 1 à 10
      recordListNumberSelect.insertAdjacentHTML(
        "beforeend",
        "<p> " + [i] + " </p>"
      );
    }

    for (let i = 0; i < scoreArray.length && i < 10; i++) {
      // Deuxième boucle for pour afficher les 10 meilleurs joueurs avec leur résultat respectif
      recordListNameSelect.insertAdjacentHTML(
        "beforeend",
        "<p>" + scoreArray[i].playername + "</p>"
      );
      recordListScoreSelect.insertAdjacentHTML(
        "beforeend",
        "<p>" + scoreArray[i].playerscore + "</p>"
      );
    }
    for (let i = 0; i < scoreArray.length && i < 10; i++) {
      // Troisième boucle for pour appliquer du style sur le score d'un joueur si c'est ce dernier qui joue actuellement
      let pNumberRecord = document.querySelectorAll(".record-list-number > p");
      let pPlayerRecord = document.querySelectorAll(".record-list-name > p");
      let pScoreRecord = document.querySelectorAll(".record-list-score > p");

      if (input.value === scoreArray[i].playername) {
        pNumberRecord[i].style.color = "#4682B4";
        pPlayerRecord[i].style.color = "#4682B4";
        pScoreRecord[i].style.color = "#4682B4";
      } else {
      }
    }
  } else {
    // On vide tous les champs lorsqu'on click à nouveau sur le bouton
    recordListNameSelect.innerHTML = "";
    recordListNumberSelect.innerHTML = "";
    recordListScoreSelect.innerHTML = "";
    recordButtonSelect.style.color = "white";
    recordButtonSelect.style.borderBottom = "1px solid white";
    recordListSelect.classList.replace("record-list-active", "record-list");
  }
});

/* On exporte les constantes et fonctions */
export {
  input,
  form,
  recordButtonSelect,
  recordListSelect,
  playerCreation,
  addPlayer,
  playerName,
  scoreArray,
  gameLaunchSelector,
};

/* Importation des paramètres nécessaires à l'exécution de score.js */
import { globalSettings } from "./setting.js";
import { scoreArray } from "./localStorage.js";

/* Fonction d'actualisatio des scores */

function updateScore() {
  document.getElementById("currentLife").style.width = `${globalSettings.currentLife}%`; // On ajoute du code html pour afficher la vie actuel du joueur
  document.getElementById("currentScore").innerHTML = `Actuel : ${globalSettings.currentScore}`; // On ajoute du code html pour afficher le score actuel

  if (scoreArray.length == 0) 
  {
    document.getElementById("bestScore").innerHTML = `Meilleur : 0`; // On ajoute du code html pour afficher le meilleur score
  } else {
    document.getElementById("bestScore").innerHTML = `Meilleur : ${scoreArray[0].playerscore}`; // On ajoute du code html pour afficher le meilleur score
  }
}

/* Exportation de la fonction pour modifier le score */
export { updateScore };

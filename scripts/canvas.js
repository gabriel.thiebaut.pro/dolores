/* Déclaration des variables pour l'utilisation de canvas */
const canvas = document.getElementById('canvas'); // On récupère le canvas dans le DOM
const ctx = canvas.getContext('2d'); // Méthode de représentation bi-dimensionnel avec canvas

/* Exportation des constantes du canvas */
export { canvas, ctx };
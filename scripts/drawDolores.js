/* Importation des paramètres nécessaires à l'exécution de drawDolores.js */
import { playerImage } from "./images.js";
import { overSound } from "./sounds.js";
import { canvas, ctx } from "./canvas.js";
import { globalSettings, size, cTenth } from "./setting.js";
import { setup } from "./setup.js";
import { addPlayer, playerName } from "./localStorage.js";
import { selectBonus } from "./classTrash.js";

/* Exportation de la fonction permettant de dessiner Dolores lorsqu'on joue */
export function drawDoloresPlay() {
  ctx.drawImage(
    playerImage,
    Math.floor(((globalSettings.index / 4) % 6) / 3) * size,
    0,
    size,
    size,
    cTenth,
    globalSettings.flyHeight,
    size,
    size
  ); // Placement en 1/10 sur l'axe horizontal selon les 2 positions du sprite
  globalSettings.flight += globalSettings.gravity;

  if (globalSettings.flyHeight >= canvas.height) {
    // Condition si la position en y du personnage passe sous le canvas
    addPlayer(playerName, globalSettings.bestScore); // On ajoute le player dans le local storage
    overSound.play(); // Son de game over
    selectBonus.innerHTML = ""; // On réinitialise le message de bonus
    globalSettings.countItems = 0; // On réinitialise le compteur bonus
    playerImage.src = "./assets/img/mermaid.png";
    globalSettings.gamePlaying = false; // Game over => on relance le setup
    setup();
  }

  // On ne peut pas aller au dessus du canvas
  globalSettings.flyHeight = Math.max(
    globalSettings.flyHeight + globalSettings.flight,
    0
  );
}

/* On exporte la fonction permettant de dessiner Dolores dans l'écran principal */
export function drawDoloresMenu() {
  ctx.drawImage(
    playerImage,
    Math.floor(((globalSettings.index / 4) % 6) / 3) * size,
    0,
    size,
    size,
    canvas.width / 2 - size / 2,
    globalSettings.flyHeight,
    size,
    size
  ); // Placement au milieu de l'écran selon les 2 positions du sprite
  globalSettings.flyHeight = canvas.height / 2 - size / 2;
}

/* Importation des paramètres nécessaires à l'exécution de setting.js */
import { Trash } from "./classTrash.js";
import {
  blackBagImage,
  bottleImage,
  beerImage,
  pizzaImage,
  leafImage,
} from "./images.js";

/* Déclarations des variables globales du jeu */
const globalSettings = {
  gamePlaying: false, // Boléeen renvoie true si on joue, false si on a perdu => menu principal
  index: 0, // L'index prend une valeur de +1 à chaque boucle de chargement
  bestScore: 0, // Meilleur score
  currentScore: 0, // Score actuel
  currentLife: 3, // Nombre de vies au début d'une partie
  flight: 0, // Distance de vol
  flyHeight: 0, // Position en y du personnage
  bags: [], // Création d'un array vide pour contenir les données des bags
  bottles: [], // Création d'un array vide pour contenir les données des bottles
  beers: [], // Création d'un array vide pour contenir les données des beers
  pizzas: [], // Création d'un array vide pour contenir les données des pizzas
  speed: 6, // Vitesse de défilement du décor
  backgroundSpeed: 3, // Vitesse du background
  defaultGap: 20, // Facteur de gap par défaut
  leaf: [], // Création d'un array vide pour contenir les données des feuilles
  isCarryingBag: false, // Boléeen renvoie true si on a "attrapé" un sac, false sinon
  gravity: 0.05, // Force qui attire le personnage vers le bas de l'écran
  countItems: 0, // Compteur d'items ramassés (bonus)
};

/* Déclarations des constantes relatives à Dolores et à la taille du background */
const size = 31; // Taille du personnage
const jump = -1.5; // Force qui permet au personnage de remonter vers la surface
const cTenth = canvas.width / 10; // Le personnage est situé à 1/10ème de l'écran sur l'axe horizontal
const backgroundWidth = 3046; // Longueur du background

/* Déclarations des instances de classes relatives aux trash */
const bottle = new Trash( // Voir class Trash pour les paramètres (classTrash.js)
  "Bottle",
  globalSettings.defaultGap * 2,
  48,
  18,
  bottleImage,
  globalSettings.bottles,
  2,
  1
);
const beer = new Trash(
  "Beer",
  globalSettings.defaultGap * 3,
  50,
  18,
  beerImage,
  globalSettings.beers,
  10,
  1
);
const pizza = new Trash(
  "Pizza",
  globalSettings.defaultGap,
  37,
  62,
  pizzaImage,
  globalSettings.pizzas,
  5,
  1
);
const bag = new Trash(
  "Bag",
  globalSettings.defaultGap * 1,
  65,
  65,
  blackBagImage,
  globalSettings.bags,
  3,
  2
);
const leaf = new Trash(
  "Leaf",
  globalSettings.defaultGap * 5,
  35,
  35,
  leafImage,
  globalSettings.leaf,
  3,
  10
);

/* Exportation des instances de classes relatives aux trash */
export {
  beer,
  pizza,
  bag,
  bottle,
  leaf,
  globalSettings,
  size,
  jump,
  cTenth,
  backgroundWidth,
};

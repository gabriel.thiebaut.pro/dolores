/* Importation des paramètres nécessaires à l'exécution de drawBackground.js */
import { globalSettings, backgroundWidth } from "./setting.js";
import { canvas, ctx } from "./canvas.js";
import { backgroundImage } from "./images.js";

/* Exportation de la fonction permettant de dessiner le fond d'écran */
export function drawBackground() {
  ctx.drawImage(
    // On affiche la première partie du fond positionné en x à + backgroundWidth par rapport au deuxième fond (illusion d'enchainement du fond)
    backgroundImage,
    0,
    0,
    backgroundWidth,
    canvas.height,
    -(
      (globalSettings.index * globalSettings.backgroundSpeed) %
      backgroundWidth
    ) + backgroundWidth,
    0,
    backgroundWidth,
    canvas.height
  );
  ctx.drawImage(
    // On affiche la deuxième partie du fond
    backgroundImage,
    0,
    0,
    backgroundWidth,
    canvas.height,
    -(
      (globalSettings.index * globalSettings.backgroundSpeed) %
      backgroundWidth
    ),
    0,
    backgroundWidth,
    canvas.height
  );
}

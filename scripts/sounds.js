/* Importation des fichiers audios avec l'objet JS Audio */
const addPoint = new Audio("./assets/sounds/addpoint.mp3");
addPoint.volume = 0.1;

const backgroundMusic = new Audio("./assets/sounds/backgroundmusic.mp3");
backgroundMusic.loop = true;

const hitSound = new Audio("./assets/sounds/hitbag.mp3");
hitSound.volume = 0.5;

const healthSound = new Audio("./assets/sounds/healthSound.mp3");

const overSound = new Audio("./assets/sounds/over.mp3");

/* Exportation des fichiers audio */
export { addPoint, backgroundMusic, hitSound, overSound, healthSound };
